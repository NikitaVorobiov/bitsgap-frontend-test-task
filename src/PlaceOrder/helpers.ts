import {ProfitTarget} from './model';

export const calculateTradePrice = (profit: number, price: number, orderSide: string) =>
    orderSide === 'buy' ? price + price * profit : price - price * profit;

export const calculateProfit = (tradePrice: number, price: number) =>
    Number(((tradePrice / price) - 1).toFixed(2));

export const calculateTradeProfit = (formPrice: number, targetPrice: number, amount: number,formAmount: number, orderSide: string) =>
    amount * formAmount * (orderSide === 'buy' ? (targetPrice - formPrice) : (formPrice - targetPrice));

export const calculateTotalProfitAmount = (profits: Array<ProfitTarget>) => Number(profits.reduce((acc, {amount}) => {
    return acc + amount
}, 0).toFixed(2))

export const calculateMaxAmount = (profits: Array<ProfitTarget>) =>
    Math.max(...profits.map(target => target.amount));

import React, {FormEvent} from "react";
import { observer } from "mobx-react";
import block from "bem-cn-lite";
import {assoc} from 'ramda';

import { NumberInput, Button } from "components";

import { BASE_CURRENCY, QUOTE_CURRENCY } from "./constants";
import { useStore } from "./context";
import { PlaceOrderTypeSwitch } from "./components/PlaceOrderTypeSwitch/PlaceOrderTypeSwitch";
import { TakeProfit } from "./components/TakeProfit/TakeProfit";
import { ProfitTarget } from "./model";
import { calculateTotalProfitAmount } from "./helpers";
import "./PlaceOrderForm.scss";

const b = block("place-order-form");

export const PlaceOrderForm = observer(() => {
  const {
    activeOrderSide,
    price,
    total,
    amount,
    setPrice,
    setAmount,
    setTotal,
    setOrderSide,
    profits,
    setProfitsError,
  } = useStore();

  const submitForm = (e: FormEvent) => {
     const isProfitsAmountMoreThan5 = profits.reduce((acc, {profit}) => {
         return acc + profit
     }, 0) > 5;

     const profitsLessThanMinimumValue = profits.filter(({profit}) => {
         return profit < 0.01;
     })

      const tradePricesLessThanZero = profits.filter(({tradePrice}) => {
            return tradePrice <= 0;
      })

      let notGreaterProfitsValue: Array<ProfitTarget> = [];

      profits.reduce((acc, target) => {
          if(acc > target.profit) {
              notGreaterProfitsValue = [...notGreaterProfitsValue, target]
          }

          return acc = target.profit
      }, profits[0].profit)

      const totalProfitAmount = calculateTotalProfitAmount(profits);
      let targetWithMaxAmount: any;
      if(totalProfitAmount > 1) {
          targetWithMaxAmount = {...profits[0]};
          profits.forEach((target) => {
              if(target.amount > targetWithMaxAmount?.amount) {
                  targetWithMaxAmount = {...target};
              }
          });
      }
      if(
          isProfitsAmountMoreThan5 ||
          profitsLessThanMinimumValue.length ||
          tradePricesLessThanZero.length ||
          notGreaterProfitsValue.length ||
          targetWithMaxAmount
      ) {
          const notGreaterProfitsErrors = {
              ...notGreaterProfitsValue.reduce((acc: any, {key}) => {
                  acc[key] = { profit: true }
                  return acc;
              }, {})
          }

          const profitsErrors = targetWithMaxAmount ?
              assoc(targetWithMaxAmount.key, { amount: totalProfitAmount - 1 }, notGreaterProfitsErrors) :
              notGreaterProfitsErrors;

          setProfitsError(profitsErrors);
          e.preventDefault()
          return;
      }

      //@TODO Send request
  }

  return (
    <form className={b()}>
      <div className={b("header")}>
        Binance: {`${BASE_CURRENCY} / ${QUOTE_CURRENCY}`}
      </div>
      <div className={b("type-switch")}>
        <PlaceOrderTypeSwitch
          activeOrderSide={activeOrderSide}
          onChange={setOrderSide}
        />
      </div>
      <div className={b("price")}>
        <NumberInput
          label="Price"
          value={price}
          onChange={(value) => setPrice(Number(value))}
          InputProps={{ endAdornment: QUOTE_CURRENCY }}
        />
      </div>
      <div className={b("amount")}>
        <NumberInput
          value={amount}
          label="Amount"
          onChange={(value) => setAmount(Number(value))}
          InputProps={{ endAdornment: BASE_CURRENCY }}
        />
      </div>
      <div className={b("total")}>
        <NumberInput
          value={total}
          label="Total"
          onChange={(value) => setTotal(Number(value))}
          InputProps={{ endAdornment: QUOTE_CURRENCY }}
        />
      </div>
      <div className={b("take-profit")}>
        <TakeProfit orderSide={activeOrderSide} />
      </div>
      <div className="submit">
        <Button
          color={activeOrderSide === "buy" ? "green" : "red"}
          type="button"
          fullWidth
          onClick={submitForm}
        >
          {activeOrderSide === "buy"
            ? `Buy ${BASE_CURRENCY}`
            : `Sell ${QUOTE_CURRENCY}`}
        </Button>
      </div>
    </form>
  );
});

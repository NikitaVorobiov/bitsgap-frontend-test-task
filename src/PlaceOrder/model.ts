export type OrderSide = "buy" | "sell";

export type ProfitTarget = {
    profit: number;
    tradePrice: number;
    amount: number;
    key: string;
};

type TargetError = {
    profit?: boolean,
    amount?: number,
};

export type ProfitTargetErrors = {
    [key: string]: TargetError
};

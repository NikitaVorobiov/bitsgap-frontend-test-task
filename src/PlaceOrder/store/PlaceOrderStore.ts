import { observable, computed, action } from "mobx";

import { OrderSide, ProfitTarget, ProfitTargetErrors } from "../model";

export class PlaceOrderStore {
  @observable activeOrderSide: OrderSide = "buy";
  @observable price: number = 0;
  @observable amount: number = 0;
  @observable profits: Array<ProfitTarget> = [];
  @observable isSend: boolean = false;
  @observable profitsTargetErrors: ProfitTargetErrors = {
    targets: {}
  };

  @computed get total(): number {
    return this.price * this.amount;
  }

  @action.bound
  public setOrderSide(side: OrderSide) {
    this.activeOrderSide = side;
  }

  @action.bound
  public setPrice(price: number) {
    this.price = price;
  }

  @action.bound
  public setAmount(amount: number) {
    this.amount = amount;
  }

  @action.bound
  public setTotal(total: number) {
    this.amount = this.price > 0 ? total / this.price : 0;
  }

  @action.bound
  public setProfit(takeProfits: Array<ProfitTarget>) {
    this.profits = takeProfits;
  }

  @action.bound
  public setProfitsError(targets: ProfitTargetErrors) {
    this.isSend = true;
    this.profitsTargetErrors = targets;
  }

  @action.bound
  public resetProfitsError() {
    this.isSend = false;console.log
    this.profitsTargetErrors = {
      targets: {},
    };
  }
}

/* eslint @typescript-eslint/no-use-before-define: 0 */

import React, {useState, useEffect} from "react";
import block from "bem-cn-lite";
import { AddCircle } from "@material-ui/icons";
import {v4 as uuidv4} from 'uuid';
import { observer } from "mobx-react";

import { Switch, TextButton } from "components";

import { QUOTE_CURRENCY } from "../../constants";
import ProfitInputs from "../ProfitInputs/ProfitInputs";
import ProfitTitles from "../ProfitTitles/ProfitTitles";
import { useStore } from "../../context";
import { OrderSide, ProfitTarget } from "../../model";
import {
    calculateTradeProfit,
    calculateTradePrice,
    calculateTotalProfitAmount,
    calculateMaxAmount,
} from "../../helpers";

import "./TakeProfit.scss";

export const b = block("take-profit");

type Props = {
  orderSide: OrderSide;
  // ...
};

const initialState = {
    profit: 0.02,
    amount: 1,
}

const TakeProfit = observer(({ orderSide }: Props) => {
    const {price, amount, profits, setProfit} = useStore();
    const [isChecked, setIsChecked] = useState(false);

    useEffect(() => {
        setProfit(profits.map((target) => ({
            ...target,
            tradePrice: calculateTradePrice(target.profit, price, orderSide)
        })))
    }, [price, orderSide])

    const removeProfitTarget = (targetKey: string) => {
        setProfit(profits.filter(({key}) => key !== targetKey));
    };

    const updateTargets = (target: ProfitTarget) => {
        const targetIndex = profits.findIndex(({key}) => key === target.key);

        const newProfitTargets = [...profits];
        newProfitTargets[targetIndex] = target;
        setProfit(newProfitTargets);
    }

    const addProfitTarget = () => {
        const {profit: lastProfit} = profits[profits.length - 1];
        const profit = lastProfit + 0.02;

        const newTarget = {
            profit,
            tradePrice: calculateTradePrice(profit, price, orderSide),
            amount: 0.2,
            key: uuidv4(),
        }

        const totalProfitAmount = calculateTotalProfitAmount(profits);

        if (totalProfitAmount > 0.8) {
            const maxAmount = calculateMaxAmount(profits);

            const newProfitTargets = profits.map((target) => {
                return {
                    ...target,
                    amount: target.amount === maxAmount ? Number((target.amount - 0.2).toFixed(2)) : target.amount
                }
            })

            return setProfit([...newProfitTargets, newTarget])
        }

        setProfit([...profits, newTarget]);
    };

    const setIsProfitActive = (active: boolean) => {
        setProfit([{
            ...initialState,
            tradePrice: calculateTradePrice(initialState.profit, price, orderSide),
            key: uuidv4(),
        }])

        setIsChecked(active);
    }

    return (
        <div className={b()}>
            <div className={b("switch")}>
                <span>Take profit</span>
                <Switch checked={isChecked} onChange={setIsProfitActive}/>
            </div>
            {isChecked &&
            <div className={b("content")}>
                <ProfitTitles />
                {profits.map((target) => (
                    <ProfitInputs
                        key={target.key}
                        target={target}
                        updateTargets={updateTargets}
                        removeTarget={removeProfitTarget}
                    />))}
                {profits.length <= 4 && <TextButton onClick={addProfitTarget} className={b("add-button")}>
                    <AddCircle className={b("add-icon")}/>
                    <span>Add profit target {profits.length}/5</span>
                </TextButton>}
                <div className={b("projected-profit")}>
                    <span className={b("projected-profit-title")}>Projected profit</span>
                    <span className={b("projected-profit-value")}>
            <span>{profits.reduce((acc, {tradePrice, amount: tradeAmount}) => {
                return acc + calculateTradeProfit(price, tradePrice, tradeAmount, amount, orderSide)
            }, 0).toFixed(2)}</span>
            <span className={b("projected-profit-currency")}>
              {QUOTE_CURRENCY}
            </span>
          </span>
                </div>
            </div>}

        </div>
    );
})

export { TakeProfit };

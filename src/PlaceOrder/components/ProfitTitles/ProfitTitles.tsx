import React from "react";
import {b} from "../TakeProfit/TakeProfit";
import {useStore} from "../../context";

export const ProfitTitles = () => {
    const { activeOrderSide } = useStore();
    return (
        <div className={b("titles")}>
            <span>Profit</span>
            <span>Trade price</span>
            <span>Amount to {activeOrderSide === "buy" ? "sell" : "buy"}</span>
        </div>
    );
};

export default ProfitTitles;
import React from "react";
import {Cancel} from "@material-ui/icons";
import { observer } from "mobx-react";

import {NumberInput} from "../../../components";
import {b} from "../TakeProfit/TakeProfit";
import {QUOTE_CURRENCY} from "../../constants";
import {ProfitTarget} from '../../model'
import {calculateProfit, calculateTradePrice} from '../../helpers'
import {useStore} from "../../context";

type Props = {
    target: ProfitTarget,
    updateTargets: (target: ProfitTarget) => void,
    removeTarget: (key: string) => void,
}

const ProfitInputs = observer(({target, updateTargets, removeTarget} :Props) => {
    const {profit, amount, tradePrice, key} = target;
    const {
        price,
        activeOrderSide,
        profitsTargetErrors,
        resetProfitsError,
        isSend
    } = useStore();

    const changeTarget = (
        value: number,
        key?: ProfitTarget[keyof ProfitTarget]) => {
        if (isSend) {
            resetProfitsError()
        }

        if (key === 'profit') {
            return updateTargets({
                ...target,
                profit: value,
                tradePrice: calculateTradePrice(value , price, activeOrderSide) })
        } else if (key === 'tradePrice') {
            return updateTargets({
                ...target,
                profit: calculateProfit(value, price),
                tradePrice: value,
            })
        }

        return updateTargets({
            ...target,
            amount: value,
        })
    }

    return (
        <div className={b("inputs")}>

                <NumberInput
                    error={profit > 5 &&
                    'Maximum profit sum is 500%' ||
                    profitsTargetErrors[key]?.profit &&
                    'Each target\'s profit should be greater than the previous one'
                    }
                    onChange={(value) => changeTarget(value ? value / 100 : 0, 'profit')}
                    value={profit * 100}
                    decimalScale={2}
                    InputProps={{ endAdornment: "%" }}
                    variant="underlined"
                />

                <NumberInput
                    onChange={(value) => changeTarget(value ?? 0, 'tradePrice')}
                    value={tradePrice}
                    decimalScale={2}
                    InputProps={{ endAdornment: QUOTE_CURRENCY }}
                    variant="underlined"
                    error={isSend && 'Minimum value is 0.01'}
                />

                <NumberInput
                    onChange={(value) => changeTarget(value ? value / 100 : 0)}
                    value={amount * 100}
                    decimalScale={2}
                    InputProps={{ endAdornment: "%" }}
                    variant="underlined"
                    // @ts-ignore
                    error={profitsTargetErrors[key]?.amount && `${amount} out of 100% selected. Please decrease by ${profitsTargetErrors[key]?.amount * 100}`}
                />

            <div className={b("cancel-icon")}>
                <Cancel onClick={() => removeTarget(key)} />
            </div>
        </div>
    );
})

export default ProfitInputs;
